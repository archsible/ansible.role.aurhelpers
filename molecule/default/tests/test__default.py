import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_yay_is_installed(host):
    yay = host.package("yay")
    assert yay.is_installed


def test_pacaur_is_installed(host):
    pacaur = host.package("pacaur")
    assert pacaur.is_installed


def test_pacget_is_installed(host):
    pacget = host.package("pacget")
    assert pacget.is_installed
