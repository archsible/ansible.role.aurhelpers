
.. image:: https://img.shields.io/gitlab/pipeline/archsible/ansible.role.aurhelpers/master
        :target: https://gitlab.com/archsible/ansible.role.aurhelpers/pipelines

Role Name
=========

aurhelpers


Installs different AUR helpers

Requirements
------------

None

Role Variables
--------------

     vars:
       aurhelpers:
         handlers:
           - auracle-git
           - pacaur
           - pacget
           - yay

Dependencies
------------

None

Example Playbook
----------------


    - hosts: servers
      roles:
         - archsible.aurhelpers

License
-------

BSD

Author Information
------------------

Azul
